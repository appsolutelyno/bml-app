import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.breakingmarathonlimits.pacingapp',
  appName: 'Breaking Marathon Limits',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
