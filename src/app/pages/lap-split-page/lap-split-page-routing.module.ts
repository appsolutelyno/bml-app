import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LapSplitPagePage } from './lap-split-page.page';

const routes: Routes = [
  {
    path: '',
    component: LapSplitPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LapSplitPagePageRoutingModule {}
