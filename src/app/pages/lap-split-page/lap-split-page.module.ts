import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { BrMaskerModule } from 'br-mask';

import { LapSplitPagePageRoutingModule } from './lap-split-page-routing.module';

import { LapSplitPagePage } from './lap-split-page.page';
import { LapSplitComponentModule } from 'src/app/components/lap-split/lap-split.module';
import { ApplicationPipesModule } from 'src/app/pipes/application-pipes.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LapSplitPagePageRoutingModule,
    LapSplitComponentModule,
    ApplicationPipesModule,
    BrMaskerModule
  ],
  declarations: [LapSplitPagePage]
})
export class LapSplitPagePageModule {}
