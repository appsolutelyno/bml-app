import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-workout-detail',
  templateUrl: './workout-detail.page.html',
  styleUrls: ['./workout-detail.page.scss'],
})
export class WorkoutDetailPage implements OnInit {
  backButtonText: string = "Workouts";

  title: string;
  workout: string;

  constructor(private route: ActivatedRoute) {
    this.workout = this.route.snapshot.paramMap.get('id')
  }

  ngOnInit() {
    if (this.workout == "0"){
      this.title = "1500m - 1000m - 500m";
    } else if (this.workout == "1"){
      this.title = "10 x 1000m"
    };
  }

}
