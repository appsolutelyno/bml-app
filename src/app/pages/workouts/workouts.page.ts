import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.page.html',
  styleUrls: ['./workouts.page.scss'],
})
export class WorkoutsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openWorkoutPage(item: number){
    this.router.navigate(['tabs/workouts/workout-detail/', item.toString()]);
  }

}