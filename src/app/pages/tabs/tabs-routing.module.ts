import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'calculator',
        loadChildren: () => import('../calculator/calculator.module').then( m => m.CalculatorPageModule)
      },
      {
        path: 'lap-split-page',
        loadChildren: () => import('../lap-split-page/lap-split-page.module').then( m => m.LapSplitPagePageModule)
      },
      {
        path: 'workouts',
        children: [
          {
            path: '',
            loadChildren: () => import('../workouts/workouts.module').then( m => m.WorkoutsPageModule)
          },{
            path: 'workout-detail',
            loadChildren: () => import('../workout-detail/workout-detail.module').then( m => m.WorkoutDetailPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/calculator',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/calculator',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
