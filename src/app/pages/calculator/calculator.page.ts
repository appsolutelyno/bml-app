import { ChangeDetectorRef, Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import {FocusMonitor, FocusOrigin} from '@angular/cdk/a11y';
import { BmlInputComponent } from 'src/app/components/bml-input/bml-input.component';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.page.html',
  styleUrls: ['./calculator.page.scss']
})
export class CalculatorPage implements OnInit {
  @ViewChild(BmlInputComponent) bmlInput: BmlInputComponent;

  @ViewChild('subtree') subtree: ElementRef<HTMLElement>;
  subtreeOrigin = this.formatOrigin(null);

  constructor(private _focusMonitor: FocusMonitor,
    private _cdr: ChangeDetectorRef,
    private _ngZone: NgZone) {}

  ngOnInit(){}

  ngAfterViewInit(){
    this._focusMonitor.monitor(this.subtree, true)
        .subscribe(origin => this._ngZone.run(() => {
          this.subtreeOrigin = this.formatOrigin(origin);
          this._cdr.markForCheck();
          let activeInputName = document.activeElement.getAttribute("name");
          if (activeInputName != null){
            this.updateBmlInput(activeInputName);
            console.log(activeInputName);
            
          }
        }));
  }

  ngOnDestroy() {
    this._focusMonitor.stopMonitoring(this.subtree);
  }

  formatOrigin(origin: FocusOrigin): string {
    return origin ? origin + ' focused' : 'blurred';
  }
  
  updateBmlInput(type: string){
    this.bmlInput.setType(type);
  }

  keyboardBarButtonPressed(value: string){
    console.log(value);
  }

}
