import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalculatorPageRoutingModule } from './calculator-routing.module';

import { CalculatorPage } from './calculator.page';
import { KmtToMinkmComponentModule } from 'src/app/components/kmt-to-minkm/kmt-to-minkm.module';
import { MinsPrKmMinsPrMileComponentModule } from 'src/app/components/mins-pr-km-mins-pr-mile/mins-pr-km-mins-pr-mile.module';
import { BmlInputComponentModule } from 'src/app/components/bml-input/bml-input.module';
import { PaceCalculatorComponentModule } from 'src/app/components/pace-calculator/pace-calculator.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalculatorPageRoutingModule,
    KmtToMinkmComponentModule,
    MinsPrKmMinsPrMileComponentModule,
    BmlInputComponentModule,
    PaceCalculatorComponentModule
  ],
  declarations: [CalculatorPage]
})
export class CalculatorPageModule {}
