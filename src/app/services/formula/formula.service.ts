import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormulaService {

  constructor() { }
  
  // min/mile to min/km
  minMileToMinKm(value:string){
    let kmt = this.minKmToKmT(value) * 1.60934;
    console.log(kmt);
    
    let result = this.kmtToMinKm(kmt);
    console.log(result);
    
    return result;
  }

  // min/km to min/mile
  minKmToMinMile(value: string){
    let mph = this.minKmToKmT(value) / 1.60934;
    let result = this.kmtToMinKm(mph);
    return result;
  }


  // 0.0 km/t til 0:00 min/km
  kmtToMinKm(str: any) {
    if(str == null) return null;
    
    let value = str.toString();
    
    let minutesFull = this.round(((1 / value.replace(',', '.')) * 60), 2).toString();
    let minutesSplit = minutesFull.split('.');

    let minutes = +minutesSplit[0];

    let secounds = this.round(+minutesSplit[1] / 100 * 60, 0);    

    if (Number.isNaN(secounds) && Number.isNaN(minutes)) {
      return null;
    } else if (minutes == Infinity){
      return null;
    } else if (Number.isNaN(secounds)) {
      return minutes + ':00'
    } else if (Number.isNaN(minutes)) {
      if (secounds < 10) return '0:0' + secounds
      return '0:' + secounds
    }else {
      if (secounds < 10) return minutes + ':0' + secounds
      return minutes + ':' + secounds;
    }

  }

  // min/km 0:00 til km/t
  minKmToKmT(value: string) {
    if(value == "" || value == null || value == undefined) return null;
    
    let minutes = parseInt(value.split(/[:]/)[0],10);
    let seconds: number = 0;
    let secondsString = value.split(/[:]/)[1];
    
    if (secondsString.length == 1){
      seconds = parseInt(secondsString + "0");
      
    } else {
      seconds = parseInt(secondsString);
      if (Number.isNaN(seconds)) seconds = 0;
    }
    
    let timeKm = this.round((1 / ((seconds / 60) + minutes)) * 60, 1);

    if (Number.isNaN(timeKm)) {
      return 0
    } else {
      return timeKm
    }

  }

  round(value, precision) {
    var negative = false;
    if (precision === undefined) {
        precision = 0;
    }
    if (value< 0) {
        negative = true;
        value= value* -1;
    }
    var multiplicator = Math.pow(10, precision);
    value= parseFloat((value* multiplicator).toFixed(11));
    value= (Math.round(value) / multiplicator).toFixed(precision);
    if (negative) {
        value= (value* -1).toFixed(precision);
    }
    return value;
  }​
}
