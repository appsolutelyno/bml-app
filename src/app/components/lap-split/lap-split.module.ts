import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BrMaskerModule } from 'br-mask';
import { LapSplitComponent } from './lap-split.component';
import { ApplicationPipesModule } from 'src/app/pipes/application-pipes.module';


@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, BrMaskerModule, ApplicationPipesModule],
  declarations: [LapSplitComponent],
  exports: [LapSplitComponent]
})
export class LapSplitComponentModule {}
