import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

import { FormulaService } from 'src/app/services/formula/formula.service';

@Component({
  selector: 'lap-split',
  templateUrl: './lap-split.component.html',
  styleUrls: ['./lap-split.component.scss'],
})
export class LapSplitComponent implements OnInit {

  courseMeter: number = 400;
  runDistance: number;
  totalTime: any;
  totalTimeMin: number;
  totalTimeSec: number;
  timePerRound: number = 0;
  averageKmt: any = "00.0";
  averateMinKm: string = "0:00";
  timeHourMask: any = { mask: '00:00:00', len:8, type:'num' };
  keyboardShow: boolean = false;

  constructor(private formula: FormulaService, private platform: Platform) {
    this.platform.keyboardDidShow.subscribe(() => {
      this.keyboardShow = true;
    });

    this.platform.keyboardDidHide.subscribe(() => {
      this.keyboardShow = false;
    });
  }

  ngOnInit() {
    this.getCourseTime();
  }

  getCourseTime(){
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (this.totalTime != undefined) {
      hours = parseInt(this.totalTime.split(/[:]/)[0],10);
      minutes = parseInt(this.totalTime.split(/[:]/)[1],10);
      seconds = parseInt(this.totalTime.split(/[:]/)[2],10);
    }

    // Min og sek til desimal
    let totalTimeInDesimal = ((minutes+(seconds/60))/60)+hours;
    // console.log(totalTimeInDesimal + " Timer totalt")
    let resultTimeInSec = (totalTimeInDesimal/(this.runDistance/this.courseMeter))*60*60

    // Tid per runde i sek
    if(Number.isNaN(resultTimeInSec)){
      return
    } else {
      this.timePerRound = resultTimeInSec
    }

    // Avergae Km/t
    this.averageKmt = (this.runDistance/1000)/totalTimeInDesimal;

    // Min/KM
    this.averateMinKm = this.formula.kmtToMinKm(this.averageKmt);
  }

}
