import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BrMaskerModule } from 'br-mask';
import { BmlInputComponent } from './bml-input.component';


@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, BrMaskerModule],
  declarations: [BmlInputComponent],
  exports: [BmlInputComponent]
})
export class BmlInputComponentModule {}
