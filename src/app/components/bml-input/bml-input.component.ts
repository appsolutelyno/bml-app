import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'bml-input',
  templateUrl: './bml-input.component.html',
  styleUrls: ['./bml-input.component.scss'],
})
export class BmlInputComponent implements OnInit {  
  @Output() buttonValueEvent = new EventEmitter<string>();

  keyboardShow: boolean = false;
  activeTypes: Array<string>;
  _minKm: Array<string> = [
    "2:55", "3:00", "3:05", "3:10", "3:15", "3:20", "3:25", "3:30", "3:35", "3:40", "3:45", "3:50", "3:55", "4:00"
  ]
  _kmt: Array<string> = [
    "11.0", "11.5", "12.0", "12.5", "13.0", "13.5", "14.0", "14.5", "15.0", "15.5", "16.0", "16.5", "17.0", "17.5", "18.0", "18.5", "19.0", "19.5", "20.0", "20.5"
  ]
  
  predefinedTypes: Object = {
    "minKm": this._minKm,
    "kmt": this._kmt
  }

  constructor(private platform: Platform) {
    this.platform.keyboardDidShow.subscribe(() => {
      this.keyboardShow = true;
    });
    
    this.platform.keyboardDidHide.subscribe(() => {
      this.keyboardShow = false;
    });
  }
  
  ngOnInit() {
  }
  
  setType(type: string){
    this.activeTypes = this.predefinedTypes[type];
    console.log(this.predefinedTypes[type]);
  }

  onClick(value: string){
    this.buttonValueEvent.emit(value);
  }
}
