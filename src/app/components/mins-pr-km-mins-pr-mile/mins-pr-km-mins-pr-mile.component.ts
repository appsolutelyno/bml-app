import { Component, OnInit } from '@angular/core';
import { FormulaService } from 'src/app/services/formula/formula.service';

@Component({
  selector: 'mins-pr-km-mins-pr-mile',
  templateUrl: './mins-pr-km-mins-pr-mile.component.html',
  styleUrls: ['./mins-pr-km-mins-pr-mile.component.scss'],
})
export class MinsPrKmMinsPrMileComponent implements OnInit {
  minMileModel: string;
  minKmtModel: string;
  
  constructor(private formula: FormulaService) { }

  ngOnInit() {}

  minKmChange() {
    if (this.minKmtModel.length == 1) {
      this.minKmtModel += ":";
    } else if (this.minKmtModel.length > 4){
      // TODO: fix update input modal
      let newMinKmtValue = this.minKmtModel.substring(0, 4);
      this.minKmtModel = newMinKmtValue;
    }

    this.minMileModel = this.formula.minKmToMinMile(this.minKmtModel);
  }

  minMileChange() {
    if (this.minMileModel.length == 1) {
      this.minMileModel += ":";
    } else if (this.minMileModel.length > 4){
      // TODO: fix update input modal
      let newMinKmtValue = this.minMileModel.substring(0, 4);
      this.minMileModel = newMinKmtValue;
    }

    this.minKmtModel = this.formula.minMileToMinKm(this.minMileModel);
  }

}
