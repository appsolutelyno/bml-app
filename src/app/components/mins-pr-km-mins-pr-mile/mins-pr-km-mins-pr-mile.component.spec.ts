import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MinsPrKmMinsPrMileComponent } from './mins-pr-km-mins-pr-mile.component';

describe('MinsPrKmMinsPrMileComponent', () => {
  let component: MinsPrKmMinsPrMileComponent;
  let fixture: ComponentFixture<MinsPrKmMinsPrMileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MinsPrKmMinsPrMileComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MinsPrKmMinsPrMileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
