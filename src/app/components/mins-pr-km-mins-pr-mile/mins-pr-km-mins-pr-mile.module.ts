import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BrMaskerModule } from 'br-mask';
import { MinsPrKmMinsPrMileComponent } from './mins-pr-km-mins-pr-mile.component';


@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, BrMaskerModule],
  declarations: [MinsPrKmMinsPrMileComponent],
  exports: [MinsPrKmMinsPrMileComponent]
})
export class MinsPrKmMinsPrMileComponentModule {}
