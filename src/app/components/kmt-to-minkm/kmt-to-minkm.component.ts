import { Component, OnInit } from '@angular/core';
import { FormulaService } from 'src/app/services/formula/formula.service';

@Component({
  selector: 'kmt-to-minkm',
  templateUrl: './kmt-to-minkm.component.html',
  styleUrls: ['./kmt-to-minkm.component.scss'],
})
export class KmtToMinkmComponent implements OnInit {
  kmtModel: number;
  minKmtModel: string;

  // minKmMask: any = { mask: '0:00', len:4, type:'num' };
  KmtMask: any = { mask: '00,0', len:4, type:'num' };

  constructor(private formula: FormulaService) { }

  ngOnInit() {}


  // min/km til km/t
  kmTChange() {
    this.minKmtModel = this.formula.kmtToMinKm(this.kmtModel);
  }
  minKmChange() {
    if (this.minKmtModel.length == 1) {
      this.minKmtModel += ":";
    } else if (this.minKmtModel.length > 4){
      // TODO: fix update input modal
      let newMinKmtValue = this.minKmtModel.substring(0, 4);
      this.minKmtModel = newMinKmtValue;
    }
    
    this.kmtModel = this.formula.minKmToKmT(this.minKmtModel)
  }

}
