import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { KmtToMinkmComponent } from './kmt-to-minkm.component';
import { BrMaskerModule } from 'br-mask';


@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, BrMaskerModule],
  declarations: [KmtToMinkmComponent],
  exports: [KmtToMinkmComponent]
})
export class KmtToMinkmComponentModule {}
