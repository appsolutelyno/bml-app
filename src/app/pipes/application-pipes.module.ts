import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HourMinSecPipe } from './hour-min-sec/hour-min-sec.pipe';


@NgModule({
  imports: [
    CommonModule
  ],
  exports:[
    HourMinSecPipe
  ],
  declarations: [
    HourMinSecPipe
  ]
})
export class ApplicationPipesModule { }
