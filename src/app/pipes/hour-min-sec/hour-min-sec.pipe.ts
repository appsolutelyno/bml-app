import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hourMinSec'
})
export class HourMinSecPipe implements PipeTransform {
  
  transform(value: any, args?: any): any {
    if (args == "sec"){
      if (Number.isNaN(value)) return '00:00:00'
      let hour = Math.floor(value/3600);
      let minute = Math.floor((value/3600 - hour)*60);
      let sec = Math.round((((value/3600 - hour) * 60) - minute)*60)

      if (sec == 60){
        minute++
        sec = 0
      }
      
      let resultSec = ("0" + sec).slice(-2);
      let resultMinute = ("0" + minute).slice(-2);
      let resultHour = ("0" + hour).slice(-2);
      return resultHour + ":" + resultMinute + ":" + resultSec; 
    } else if (args == "ms"){
        if (Number.isNaN(value)) return '00:00:00'
        let hour = Math.floor(value/3600000);
        let minute = Math.floor((value % 3600000) / 60000);
        let sec = Math.round(((value % 360000) % 60000) / 1000);
        let ms = Math.floor(value % 360000 % 1000 % 60000 / 100);

        if (sec == 60){
          minute++
          sec = 0
        }
        
        let resultSec = ("0" + sec).slice(-2);
        let resultMinute = ("0" + minute).slice(-2);
        let resultHour = ("0" + hour).slice(-2);
        let msms = ("0" + ms).slice(-1);
        if(hour == 0){
          return resultMinute + ":" + resultSec + "." + ms; 
        } else {
          return resultHour + ":" + resultMinute + ":" + resultSec + "." + ms; 
        }
    } else {
      return null;
    }
  }

}
